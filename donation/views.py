from django.shortcuts import render, redirect
from .models import Contributors
from .forms import forms_contributors
from django.contrib.auth.models import User
from django.core import serializers
from django.http import JsonResponse, HttpResponse
import json
# Create your views here.
def donation(request):
    contributors = Contributors.objects.all()
    total = Contributors.objects.all().count()
    response = {'form':forms_contributors, 'contributors':contributors, 'total':total}
    return render(request, 'donation.html', response)

def savecontributors(request):
    form = forms_contributors(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        if request.user.is_authenticated:
            form.save()
            return redirect('/donationplaces')
        else:
            return redirect('accounts/login')
    else:
        return redirect('/donation')

def donationplaces(request):
    return render(request, 'donationplaces.html')

def carinama(request):
    contrib = Contributors.objects.all()
    json_contrib = serializers.serialize('json', contrib)
    return  HttpResponse(json_contrib, content_type='application/json')
    
    # data = json.loads(json_contrib)
    # data2 = json.dumps(data[0])
    # return JsonResponse(data2)

