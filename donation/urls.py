from donation.views import donationplaces
from django.urls import path

from . import views

app_name = 'donation'

urlpatterns = [
    path('donation', views.donation, name='donation'),
    path('savecontributors', views.savecontributors),
    path('donationplaces', views.donationplaces),
    path('carinama', views.carinama)
]