from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('kasusupdate', views.kasusupdate, name='kasusupdate'),
    path('info', views.info, name='info_prov')
]