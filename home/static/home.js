$(document).ready(() => {
    $.ajax({
        url: "/info?q=DKI Jakarta",
        async: true,
        success: function(result) {
            var view = $('#target-ajax');
            view.empty();
            for(var i = 0; i < result.length; i++) {
                if(view[i] != undefined) {
                    view.append(
                        '<tr><th>Item</th><th>Detail</th></tr>' +
                        '<tr><td>Provinsi </td>' + '<td>' + result[i]["Provinsi"] + '</td>' +
                        '<tr><td>Kasus Positif </td>' + '<td>' + result[i]["Kasus_Posi"] + '</td>' +
                        '<tr><td>Kasus Sembuh </td>' + '<td>' +result[i]["Kasus_Semb"] + '</td>'  +
                        '<tr><td>Kasus Meninggal </td>' + '<td>' + result[i]["Kasus_Meni"] + '</td>' 
                    )
                }
            }
        },
    })
    $("#search").keypress(function(e) {
        if(e.which == 13) {
            var q = e.currentTarget.value.toUpperCase();
            console.log(q)
            $.ajax({
                url: "/info?q=" + q,
                async: true,
                success: function(result) {
                    var view = $('#target-ajax');
                    view.empty();
                    for(var i = 0; i < result.length; i++) {
                        if(view[i] != undefined) {
                            view.append(
                                '<tr><th>Item</th><th>Detail</th></tr>' +
                                '<tr><td>Provinsi </td>' + '<td>' + result[i]["Provinsi"] + '</td>' +
                                '<tr><td>Kasus Positif </td>' + '<td>' + result[i]["Kasus_Posi"] + '</td>' +
                                '<tr><td>Kasus Sembuh </td>' + '<td>' +result[i]["Kasus_Semb"] + '</td>'  +
                                '<tr><td>Kasus Meninggal </td>' + '<td>' + result[i]["Kasus_Meni"] + '</td>' 
                            )
                        }
                    }
                },
            })
        }
    });
    $("#id-box").mouseenter(function() {
        var box = document.getElementById("id-box");
        box.style.width = "300px";
        box.style.textAlign = "center";
    });
    $("#id-box").mouseleave(function() {
        var box = document.getElementById("id-box");
        box.style.width = "150px";
        box.style.textAlign ="left";
    });
    $("#id-box1").mouseenter(function() {
        var box = document.getElementById("id-box1");
        box.style.width = "300px";
        box.style.textAlign = "center";
    });
    $("#id-box1").mouseleave(function() {
        var box = document.getElementById("id-box1");
        box.style.width = "150px";
        box.style.textAlign = "left";
    });
    $("#id-box2").mouseenter(function() {
        var box = document.getElementById("id-box2");
        box.style.width = "300px";
        box.style.textAlign = "center";
    });
    $("#id-box2").mouseleave(function() {
        var box = document.getElementById("id-box2");
        box.style.width = "150px";
        box.style.textAlign = "left";
    });
    $("#id-box3").mouseenter(function() {
        var box = document.getElementById("id-box3");
        box.style.width = "300px";
        box.style.textAlign = "center";
    });
    $("#id-box3").mouseleave(function() {
        var box = document.getElementById("id-box3");
        box.style.width = "150px";
        box.style.textAlign = "left";
    });
});