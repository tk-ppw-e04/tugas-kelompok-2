from django.shortcuts import render, redirect
from .models import subscribe_models, kasus_models
from .forms import form_subscribe, form_kasus
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import JsonResponse
import json
import requests

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = form_subscribe(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Thankyou for subscribe us!")

    form = form_subscribe()
    subs = subscribe_models.objects.all()
    formkasus = form_kasus()
    kasus = kasus_models.objects.all()
    context = {
        'subs': subs,
        'form' : form,
        'kasus': kasus,
        'formkasus' : formkasus,
    }
    return render(request, 'index.html' , context)

def kasusupdate(request):
    if request.method == 'POST':
        formkasus = form_kasus(request.POST)
        if formkasus.is_valid():
            if request.user.is_authenticated:
                formkasus.save()
                messages.success(request, "Update successfully.")
            else:
                return redirect('accounts/login')

    formkasus = form_kasus()
    kasus = kasus_models.objects.all()
    context = {
        'kasus': kasus,
        'formkasus' : formkasus,
    }
    return render(request, 'kasus.html' , context)

def info(request):
    lst = []
    try:
        item = request.GET['q']
    except:
        item = ""
    response = requests.get('https://api.kawalcorona.com/indonesia/provinsi/').json()
    for i in response:
        if(item.lower() in i["attributes"]["Provinsi"].lower()):
            lst.append(i["attributes"])
    # print(lst)

    return JsonResponse(lst, safe= False)