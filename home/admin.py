from django.contrib import admin

# Register your models here.
from .models import subscribe_models, kasus_models
admin.site.register(subscribe_models)
admin.site.register(kasus_models)