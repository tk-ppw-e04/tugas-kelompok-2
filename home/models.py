from django.db import models

# Create your models here.
class subscribe_models(models.Model):
    nama = models.CharField(max_length=50)
    email = models.EmailField()

    # def __str__(self):
    #     return '%s' % self.nama

class kasus_models(models.Model):
    total = models.IntegerField()
    dirawat = models.IntegerField()
    sembuh = models.IntegerField()
    meninggal = models.IntegerField()
    update = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return '%d' % self.total