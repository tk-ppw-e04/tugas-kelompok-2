from django.test import TestCase, Client
from .models import subscribe_models, kasus_models
from .forms import form_subscribe, form_kasus
from django.http import response
from django.urls.base import resolve
from . import views
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

# Create your tests here.
class HomeUnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_func_index(self):
        found = resolve('/')
        self.assertEqual(found.func, views.index)

    def test_template_home(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_view_subscribe(self):
        response = Client().get('/')
        isi_html = response.content.decode('utf8')
        self.assertIn("SUBSCRIBE TO COVCARE", isi_html)

    def test_models_subscribe(self):
        subscribe_models.objects.create(nama='John' , email='john@example.com')
        subs = subscribe_models.objects.all().count()
        self.assertEqual(subs,1)

    def test_form_valid(self):
        data = {'nama':'John','email':'john@example.com'}
        form = form_subscribe(data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['nama'],"John")
        self.assertEqual(form.cleaned_data['email'],"john@example.com")

    def test_post_form_subscribe(self):
        test_nama = 'John'
        test_email = 'john@example.com'
        response = self.client.post('/' , {'nama' : test_nama , 'email' : test_email})
        self.assertEqual(response.status_code,200)
        form = form_subscribe(data={'nama' : test_nama , 'email' : test_email})
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['nama'],"John")
        self.assertEqual(form.cleaned_data['email'],"john@example.com")

    def test_kasusupdate_url_is_exist(self):
        response = Client().get('/kasusupdate')
        self.assertEqual(response.status_code, 200)

    def test_kasusupdate_using_template(self):
        response = Client().get('/kasusupdate')
        self.assertTemplateUsed(response, 'kasus.html')

    def test_func_kasusupdate(self):
        found = resolve('/kasusupdate')
        self.assertEqual(found.func, views.kasusupdate)

    def test_models_kasus(self):
        kasus_models.objects.create(total = 1000 , dirawat = 1000 , sembuh = 1000 , meninggal = 1000)
        kasus = kasus_models.objects.all().count()
        self.assertEqual(kasus,1)   

    def test_post_form_kasus(self):
        user = User.objects.create(username='test')
        user.set_password('secret000')
        user.save()
        test_total = 1000
        test_dirawat = 1000
        test_sembuh = 1000
        test_meninggal = 1000
        self.client.login(username='test', password='secret000')
        self.client.post('/kasusupdate' , {'total' : test_total , 'dirawat' : test_dirawat , 'sembuh' : test_sembuh , 'meninggal' : test_meninggal})
        total = kasus_models.objects.all().count()
        self.assertEqual(total, 1)

    def test_form_kasus_before_login(self):
        test_total = 1000
        test_dirawat = 1000
        test_sembuh = 1000
        test_meninggal = 1000
        self.client.login(username='test', password='secret0000')
        self.client.post('/kasusupdate' , {'total' : test_total , 'dirawat' : test_dirawat , 'sembuh' : test_sembuh , 'meninggal' : test_meninggal})
        total = kasus_models.objects.all().count()
        self.assertEqual(total, 0)

    def test_url_kasus_provinsi(self):
        response = Client().get('/info')
        self.assertEqual(response.status_code, 200)        

