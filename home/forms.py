from django import forms
from .models import subscribe_models, kasus_models

class form_subscribe(forms.ModelForm):
    class Meta:
        model = subscribe_models
        fields = '__all__'
    nama = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Input your name here'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'placeholder':'Input your e-mail here'}))

class form_kasus(forms.ModelForm):
    class Meta:
        model = kasus_models
        fields = '__all__'
    