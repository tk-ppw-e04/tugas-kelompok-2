# Tugas Kelompok

[![pipeline status](https://gitlab.com/tk-ppw-e04/tugas-kelompok-2/badges/master/pipeline.svg)](https://gitlab.com/tk-ppw-e04/tugas-kelompok-2/commits/master)
[![coverage report](https://gitlab.com/tk-ppw-e04/tugas-kelompok-2/badges/master/coverage.svg)](https://gitlab.com/tk-ppw-e04/tugas-kelompok-2/commits/master/coverage.svg)


## Member:
- Fayza Azzahra Robby (1906305934)
- Karina Aulia Putri (1906298954)
- Keiko Aaliya Hernowo Putri (1906298960)
- Mirza Raevan Faisal (1906303052)

## Link Heroku:
https://covcare-id.herokuapp.com/

## Description:
COVCARE merupakan website peduli COVID-19, yang hingga kini menjadi topik utama di seluruh dunia, di COVCARE user akan mendapatkan berbagai macam informasi seputar COVID-19 dan juga dapat mengetahui donasi yang dibuka di suatu daerah secara spesifik. 

## Apps Feature:
1. Subscription\
User perlu memasukkan nama dan alamat e-mailnya pada halaman utama COVCARE. Harapannya dengan fitur ini para user akan mendapatkan informasi terkini terkait COVID-19.
2. Sharing\
Pada fitur ini user dapat berbagi pengalaman seputar COVID-19 yang nantinya semua pengalaman yang tersubmit akan ditampilkan pada halaman website.
3. Donation\
Fitur ini menyediakan form donasi berisi nama dan kota user yang akan berdonasi. Setelah user mengisi form, user akan diarahkan ke halaman lain untuk memilih berdonasi ke mana, kemudian nama dan kota dari user akan ditampilkan di halaman donasi.
4. Hotline\
Fitur ini akan menampilkan hotline COVID-19 yang disediakan pemerintah dan juga hotline dari setiap rumah sakit rujukan COVID-19. User juga dapat menambahkan informasi hotline dari rumah sakit lainnya yang belum terdaftar pada database kami.


