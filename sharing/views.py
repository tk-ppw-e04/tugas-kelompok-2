from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
from .models import Pengalaman
from .forms import SharingForm
from django.core import serializers

# Create your views here.

def sharing_page(request):
    sharing = Pengalaman.objects.all()
    form = SharingForm()
    if request.method == 'POST':  
        forminput = SharingForm(request.POST)
        if forminput.is_valid():
            if request.user.is_authenticated:
                forminput.save()
                return redirect('/sharing-result')
            else:
                return redirect('accounts/login')
        else:
            return render(request, 'sharing-form.html', {'form':form, 'status': 'failed', 'data' : sharing})       
    else:
        current_data = Pengalaman.objects.all()
        return render(request, 'sharing-form.html', {'form':form, 'data':current_data})

def sharing_result(request):
    sharing = Pengalaman.objects.all()
    qs_json = serializers.serialize('json', sharing)
    return render(request, 'sharing-data.html',{'data':sharing, "chartData":qs_json} )

def sharing_search(request):
    pengalaman = Pengalaman.objects.all()
    response = {'form':SharingForm, 'contributors':pengalaman}
    return render(request, 'sharing-search.html',response)

def cari_pengalaman(request):
    pengalaman = Pengalaman.objects.all()
    json_pengalaman = serializers.serialize('json', pengalaman)
    return  HttpResponse(json_pengalaman, content_type='application/json')