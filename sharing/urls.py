from django.urls import path
from . import views

app_name = 'sharing'

urlpatterns = [
    path('sharing', views.sharing_page, name='sharing'),
    path('sharing-result', views.sharing_result, name='sharing-result'),
    path('sharing-search', views.sharing_search, name='sharing-search'),
    path('caripengalaman', views.cari_pengalaman)
]