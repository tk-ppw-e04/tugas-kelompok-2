from django import forms
from .models import Pengalaman
from django.contrib.admin import models
from .models import AGE_CHOICES, GENDER_CHOICES


class SharingForm(forms.ModelForm):

    class Meta:
        model = Pengalaman
        fields = '__all__'
        
    widgets = {
         'age':forms.Select(choices=AGE_CHOICES, attrs={'class':'form-control'}),
         'gender':forms.Select(choices=GENDER_CHOICES, attrs={'class':'form-control'}),
         'pengalaman':forms.Textarea(attrs={'class':'form-control'})
    }