from django.db import models

# Create your models here.

AGE_CHOICES = [
        ('<14','14 tahun atau kurang'),
        ('15-49','15 sampai 49 tahun'),
        ('50-64','50 sampai 64 tahun'),
        ('>65','65 tahun atau lebih'),
        ]

GENDER_CHOICES = [
        ('laki-laki','Laki-Laki'),
        ('perempuan','Perempuan')
        ]

class Pengalaman(models.Model) :
    age = models.CharField('Berapa umur Anda?', choices=AGE_CHOICES, max_length=200, null=True)
    gender = models.CharField('Apa jenis kelamin Anda?', choices=GENDER_CHOICES, max_length=200, null=True)
    pengalaman = models.TextField('Bagikan pengalaman Anda seputar COVID-19!',null=True)