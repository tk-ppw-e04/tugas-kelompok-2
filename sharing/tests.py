from django.http import response
from django.urls.base import resolve
from django.test import TestCase, Client
from . import views
from .models import Pengalaman
from .forms import SharingForm
from django.contrib.auth.models import User

# Create your tests here.
class SharingUnitTest(TestCase):
    def test_sharing_url_is_exist(self):
        response = Client().get('/sharing')
        self.assertEqual(response.status_code, 200)

    def test_sharing_using_func(self):
        found = resolve('/sharing')
        self.assertEqual(found.func, views.sharing_page)

    def test_model_create_sharing(self):
        Pengalaman.objects.create(age='15-49', gender='pria', )
        count_sharing = Pengalaman.objects.all().count()
        self.assertEquals(count_sharing,1)
    
    def test_data_kegiatan_using_template(self):
        response = Client().get('/sharing')
        self.assertTemplateUsed(response,'sharing-form.html')
    
    def test_sharing_content(self):
        response = Client().get('/sharing')
        self.assertTemplateUsed(response, 'sharing-form.html', 'base.html')
        sharing_content = response.content.decode('utf8')
        self.assertIn("Sharing", sharing_content)
        self.assertIn('Bagikan pengalaman Anda seputar COVID-19', sharing_content)
        self.assertIn('<form method="POST">', sharing_content)
        self.assertIn('Lihat Kumpulan Sharing', sharing_content)

    def test_new_form_validation(self):
        form_data = {'age':'15-49', 'gender':'laki-laki', 'pengalaman':'tes'}
        form = SharingForm(data = form_data)
        self.assertTrue(form.is_valid())

    def test_sharing_result_url_is_exist(self):
        response = Client().get('/sharing-result')
        self.assertEqual(response.status_code, 200)

    def test_sharing_result_using_func(self):
        found = resolve('/sharing-result')
        self.assertEqual(found.func, views.sharing_result)

    def test_sharing_result_using_template(self):
        response = Client().get('/sharing-result')
        self.assertTemplateUsed(response,'sharing-data.html')

    def test_sharing_result_content(self):
        response = Client().get('/sharing-result')
        self.assertTemplateUsed(response, 'sharing-data.html')
        result_content = response.content.decode('utf8')
        self.assertIn('Sharing', result_content)
        self.assertIn('Kumpulan pengalaman masyarakat seputar COVID-19', result_content)

    def test_form_sharing(self):
        user = User.objects.create(username='testuser')
        user.set_password('test1234')
        user.save()
        self.client.login(username='testuser', password='test1234')
        self.client.post('/sharing', {'age':'15-49', 'gender':'laki-laki', 'pengalaman':'tes'})
        jumlah = Pengalaman.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_form_sharing_before_login(self):
        self.client.login(username='testuser', password='test1234')
        self.client.post('/sharing', {'age':'15-49', 'gender':'laki-laki', 'pengalaman':'tes'})
        jumlah = Pengalaman.objects.all().count()
        self.assertEqual(jumlah, 0)

    def test_form_sharing_invalid(self):
        self.client.post('/sharing', {})
        jumlah = Pengalaman.objects.all().count()
        self.assertEqual(jumlah, 0)


class SharingSearchUnitTest(TestCase):
    def test_sharing_search_url_is_exist(self):
        response = Client().get('/sharing-search')
        self.assertEqual(response.status_code, 200)

    def test_sharing_search_using_func(self):
        found = resolve('/sharing-search')
        self.assertEqual(found.func, views.sharing_search)

    def test_caripengalaman_is_exist(self):
        response = Client().get('/caripengalaman')
        self.assertEqual(response.status_code, 200)

    def test_caripengalaman_using_func(self):
        found = resolve('/caripengalaman')
        self.assertEqual(found.func, views.cari_pengalaman)

    def test_sharing_search_using_template(self):
        response = Client().get('/sharing-search')
        self.assertTemplateUsed(response,'sharing-search.html')

    def test_sharing_search_content(self):
        response = Client().get('/sharing-search')
        self.assertTemplateUsed(response, 'sharing-search.html')
        result_content = response.content.decode('utf8')
        self.assertIn('Cari Pengalaman Masyarakat', result_content)