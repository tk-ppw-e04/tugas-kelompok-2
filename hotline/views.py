from django.shortcuts import render, redirect
from .forms import forms_rumahsakit
from .models import rumahsakit
from django.contrib.auth.models import User
from django.http import JsonResponse
import json
import requests

# Create your views here.
def hotline(request):
    form = forms_rumahsakit()
    rumah_sakit = rumahsakit.objects.all()
    context = {
        'rumah_sakit':rumah_sakit,
        'form' : form,
    }
    return render(request, 'HotlinePage.html' , context)

def saveRumahSakit(request):
    form = forms_rumahsakit(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        if request.user.is_authenticated:
            form.save()
            return redirect('/hotline')
        else:
            return redirect('accounts/login')
    else:
        return redirect('/hotline')

def data(request):
    try:
        tes = request.GET['q']
    except:
        tes = ""

    filtered = []



    json_read = requests.get('https://dekontaminasi.com/api/id/covid19/hospitals').json()
    for i in json_read:
        if tes.lower() in i["name"].lower() or tes.lower() in i["region"].lower() or tes.lower() in i["province"].lower():
            filtered.append(i) 

    # print(json_read[0])

    return JsonResponse(filtered, safe = False)



