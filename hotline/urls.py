from django.urls import path

from . import views
app_name = 'hotline'

urlpatterns = [
    path('hotline', views.hotline, name='hotline'),
    path('hotline/data', views.data, name='rs_data'),
    path('saveRumahSakit', views.saveRumahSakit),
]