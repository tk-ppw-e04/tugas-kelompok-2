from django import forms
from .models import rumahsakit

class forms_rumahsakit(forms.ModelForm):
    class Meta:
        model = rumahsakit
        fields = [
            'nama', 'nomor'
        ]