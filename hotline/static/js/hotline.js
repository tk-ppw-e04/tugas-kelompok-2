$(document).ready(function() {
    $("#input").keypress(function(e) {
        if (e.which == 13){
            var q = e.currentTarget.value.toUpperCase();
            console.log(q)
            $.ajax({
                url: "/hotline/data?q=" + q,
                async: true,
                success: function(isiData) {
                    $('#content-rs-page-1').html('')
                    var result = '<ol type = "1">';
                    for (var i = 0; i < isiData.length; i++) {
                        if (isiData[i] !== undefined){
                            result +=  '<li>' + isiData[i]["name"] + "(" + isiData[i]["phone"] + ")" + '</li>'
                            
                        }
                        
                    }
                    if (isiData.length == 0){
                        alert("Rumah Sakit Tidak Ditemukan!")
                    }
                    console.log(isiData)
                    $('#content-rs-page-1').append(result);
                },
                error: function(error) {
                    alert("Terjadi kesalahan pada server");
                }
        })} 
    });
    $(".input").mouseenter( function() {
        var input = document.getElementById("input");
        input.style.width = "300px";
    });
    $(".input").mouseleave( function() {
        var input = document.getElementById("input");
        input.style.width = "200px";
    });
    
});