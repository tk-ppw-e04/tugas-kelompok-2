from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username','password1','password2']
    username = forms.CharField(required=True, max_length=20, widget=forms.TextInput(attrs={'placeholder' : 'Username', 'class' : 'form-control'}))
    password1 = forms.CharField(required=True, min_length=8, widget=forms.PasswordInput(attrs={'placeholder' : 'Password', 'class' : 'form-control'}))
    password2 = forms.CharField(required=True, min_length=8, widget=forms.PasswordInput(attrs={'placeholder' : 'Password Confirmation', 'class' : 'form-control'}))