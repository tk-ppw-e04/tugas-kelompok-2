from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
# Create your tests here.

class SignUpUnitTest(TestCase):
    def test_url_signup(self):
        response = Client().get('/accounts/signup')
        self.assertEquals(200,response.status_code)

    def test_nama_templates_signup(self):
        response = Client().get('/accounts/signup')
        self.assertTemplateUsed(response, 'registration/sign-up.html')

    # def setUp(self) -> None:
    #     self.username = 'testuser'
    #     self.password = 'password'

    

    def test_signup_page_view_name(self):
        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, template_name='registration/login.html')

    def setUp(self):
        self.user = User.objects.create_user(username='test', password='12test12testtest')
        self.user.save()
        total = User.objects.all().count()
        self.assertEqual(total, 1)
        
    def test_post_form(self):
        test_username = 'Dumby'
        test_password1 = 'halo12345'
        test_password2 = 'halo12345'
        response = self.client.post('/accounts/signup', {'username' : test_username , 'password1' : test_password1 , 'password2' : test_password2})
        self.assertEqual(response.status_code, 302)
        CreateUserForm(data={'username' : test_username , 'password1' : test_password1 , 'password2' : test_password2})
        total = User.objects.all().count()
        self.assertEqual(total, 2)

    
# class UserCreationFormTest(TestCase):
#     def setUp(self):
#         self.user = User.objects.create_user(username='test', password='12test12testtest')
#         self.user.save()

    # def test_form(self):
    #     data = {
    #         'username': 'testuser',
    #         'password1': 'hello123',
    #         'password2': 'hello123',
    #     }
    #     self.client.post('signup', data)
    #     jumlah = User.objects.all().count()
    #     self.assertEqual(jumlah, 1)
    