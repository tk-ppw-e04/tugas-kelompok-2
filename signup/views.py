from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
# from django.urls import reverse_lazy
# from django.views import generic

from .forms import CreateUserForm
# Create your views here.

# class sign_up(generic.CreateView):
#     form_class = UserCreationForm
#     success_url = reverse_lazy('login')
#     template_name = 'registration/sign-up.html'

def sign_up(request):
    if (request.method == 'POST'):
        form = CreateUserForm(request.POST)
        if (form.is_valid()):
            form.save()
            return redirect('login')
    else:
        form = CreateUserForm()
    response = {'form':form}
    return render(request,"registration/sign-up.html", response)